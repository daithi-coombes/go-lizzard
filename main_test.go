package main

import (
	"testing"

	"gitlab.com/daithi-coombes/go-lizzard/pkg/coingecko"
	"gotest.tools/assert"
)

func TestGetDelta(t *testing.T) {

	coinA := coingecko.Coin{"coinA", "coinA", "coinA"}
	coinB := coingecko.Coin{"coinB", "coinB", "coinB"}
	coinC := coingecko.Coin{"coinC", "coinC", "coinC"}
	coinD := coingecko.Coin{"coinD", "coinD", "coinD"}
	coinE := coingecko.Coin{"coinE", "coinE", "coinE"}
	coinF := coingecko.Coin{"coinF", "coinF", "coinF"}
	var fixtureA = coingecko.CoinList{
		coinA,
		coinB,
		coinC,
		coinE,
		coinF,
	}
	var fixtureB = coingecko.CoinList{
		coinA,
		coinB,
		coinC,
		coinD,
		coinE,
		coinF,
	}

	actual1 := GetDelta(fixtureA, fixtureB)
	actual2 := GetDelta(fixtureB, fixtureA)
	expected := coinD

	assert.Equal(t, actual1[0], expected)
	assert.Equal(t, actual2[0], expected)
}
