# go-lizzard

watch coingecko for new token


## Flow

 - get list from api,
 - read from state
 - if different alert channel

## Unit tests

 - it will get list from coingecko
    - pkg client
    - pkg coingecko
 - it will read state (use fs for now?)
    - pkg state
 - it will diff
    - pkg diff
 - it will alert delta
    - pkg alert
    - pkg whatsapp