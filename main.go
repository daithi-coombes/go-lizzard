package main

import (
	"log"
	"net/http"
	"time"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/daithi-coombes/go-lizzard/pkg/coingecko"
	"gitlab.com/daithi-coombes/go-lizzard/pkg/data"
)

var statePath = "./model/state.json"
var endpoint = "https://api.coingecko.com/api/v3"

func main() {

	worker()
	for range time.Tick(30 * time.Second) {
		worker()
	}
}

func worker() {
	// instances
	gecko := coingecko.NewGecko(&http.Client{}, endpoint)
	state := data.NewState(statePath)

	// get current coin list
	newListByte, err := gecko.GetList()
	if err != nil {
		log.Fatal(err)
	}
	var newList coingecko.CoinList
	gecko.ParseJSON(newListByte, &newList)

	// read db state
	curListByte, err := state.Read()
	if err != nil {
		log.Fatal(err)
	}
	var curList coingecko.CoinList
	gecko.ParseJSON(curListByte, &curList)

	delta := GetDelta(newList, curList)
	spew.Dump(delta)
	// fmt.Printf("curlList: %v\n", curList)

	// write db state
	if err = state.Write(newListByte); err != nil {
		log.Fatal(err)
	}
}

func GetDelta(a, b coingecko.CoinList) (diff []coingecko.Coin) {
	m := map[string]coingecko.Coin{}

	var s, l coingecko.CoinList
	if len(a) > len(b) {
		s = b
		l = a
	} else {
		s = a
		l = b
	}

	for _, item := range s {
		m[item.ID] = item
	}

	for _, item := range l {
		if _, ok := m[item.ID]; !ok {
			diff = append(diff, item)
		}
	}

	return diff
}
