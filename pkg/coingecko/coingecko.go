package coingecko

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type CoinGecko struct {
	Client   *http.Client
	Endpoint string
}

type CoinList []Coin

type Coin struct {
	ID     string `json:"id"`
	Symbol string `json:"symbol"`
	Name   string `json:"name"`
}

func NewGecko(client *http.Client, endpoint string) *CoinGecko {
	instance := &CoinGecko{client, endpoint}
	return instance
}

func (c *CoinGecko) GetList() ([]byte, error) {

	var res []byte

	resp, err := c.Client.Get(c.Endpoint + "/coins/list")
	if err != nil {
		return res, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}

	return body, nil

}

func (c *CoinGecko) ParseJSON(body []byte, i interface{}) (interface{}, error) {

	if err := json.Unmarshal(body, i); err != nil {
		return i, err
	}

	return i, nil
}
