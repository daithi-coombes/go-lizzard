package coingecko

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"

	"gotest.tools/assert"
)

func TestGetList(t *testing.T) {

	mockResult, err := loadFixture("fixtureGetCoinList.json")
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {

		_, err := rw.Write(mockResult)
		if err != nil {
			panic(err)
		}
	}))
	defer server.Close()

	client := server.Client()
	underTest := NewGecko(client, "https://api.coingecko.com/api/v3")
	actual, err := underTest.GetList()
	if err != nil {
		panic(err)
	}
	expected := []byte{}

	assert.Equal(t, true, reflect.TypeOf(actual) == reflect.TypeOf(expected))
}

func TestParseList(t *testing.T) {

	mockResult, err := loadFixture("fixtureGetCoinList.json")
	if err != nil {
		panic(err)
	}

	underTest := NewGecko(&http.Client{}, "https://api.coingecko.com/api/v3")
	actual, err := underTest.ParseJSON(mockResult, &CoinList{})
	expected := &CoinList{}
	if err != nil {
		panic(err)
	}

	assert.Equal(t, true, reflect.TypeOf(actual) == reflect.TypeOf(expected))
}

func loadFixture(filename string) ([]byte, error) {

	var b []byte
	jsonFile, err := os.Open("../../files/" + filename)
	if err != nil {
		fmt.Println(err)
		return b, err
	}
	defer jsonFile.Close()

	b, err = ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
		return b, err
	}

	return b, nil
}
