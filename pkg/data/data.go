package data

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/daithi-coombes/go-lizzard/pkg/coingecko"
)

type State struct {
	List coingecko.CoinList
	Path string
}

func NewState(path string) *State {
	return &State{coingecko.CoinList{}, path}
}

func (s *State) Write(_newState []byte) error {
	// write to json
	return ioutil.WriteFile(s.Path, _newState, 0644)
}

func (s *State) Read() ([]byte, error) {
	var res []byte

	jsonFile, err := os.Open(s.Path)
	if err != nil {
		fmt.Println(err)
		return res, err
	}
	defer jsonFile.Close()

	res, err = ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
		return res, err
	}

	return res, nil
}
