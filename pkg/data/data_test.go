package data

import (
	"fmt"
	"testing"
	"time"

	"gotest.tools/assert"
)

func TestUpdateReadState(t *testing.T) {

	expected := "{\"timestamp\":\"" + fmt.Sprintf("%d", time.Now().Unix()) + "\"}"
	underTest := NewState("../../model/state_test.json")
	if err := underTest.Write([]byte(expected)); err != nil {
		panic(err)
	}

	actual, err := underTest.Read()
	if err != nil {
		panic(err)
	}

	assert.Equal(t, string(actual), expected)
}
