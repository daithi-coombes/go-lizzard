module gitlab.com/daithi-coombes/go-lizzard

go 1.15

require (
	github.com/davecgh/go-spew v1.1.0
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.6.1
	gotest.tools v2.2.0+incompatible
)
